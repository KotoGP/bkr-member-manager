
# Table of Contents

1.  [Overview](#orgf24e906)
2.  [Current Features](#org9f656a8)
3.  [Roadmap](#org49980a3)
    1.  [<code>[0/4]</code> Members](#orgfd97cea)
        1.  [Insert, Change, Delete Members](#orgec5d586)
        2.  [Manage payments made by members](#org8f91a9f)
        3.  [Add Emergency contact information for members](#org90405f0)
        4.  [Build report of who is not up-to-date on payments](#orgebba2e1)
    2.  [<code>[0/3]</code> Merchandise](#org869d440)
        1.  [Add Vendors who we purchase from](#org67b546d)
        2.  [Allow members up to date to add orders](#orgd275c87)
        3.  [build of a report of orders from the since the last order date split report by Vendor](#org75fb447)
    3.  [<code>[0/2]</code> Events](#orge6f15e3)
        1.  [Insert, Change, Delete Events](#orgf3f561c)
        2.  [Members](#org72f8b58)



<a id="orgf24e906"></a>

# Overview

This project aims to ease the process of signing managing the motorcycle club I am a proud member of.


<a id="org9f656a8"></a>

# Current Features


<a id="org49980a3"></a>

# Roadmap


<a id="orgfd97cea"></a>

## <code>[0/4]</code> Members


<a id="orgec5d586"></a>

### [ ] Insert, Change, Delete Members


<a id="org8f91a9f"></a>

### [ ] Manage payments made by members


<a id="org90405f0"></a>

### [ ] Add Emergency contact information for members


<a id="orgebba2e1"></a>

### [ ] Build report of who is not up-to-date on payments


<a id="org869d440"></a>

## <code>[0/3]</code> Merchandise


<a id="org67b546d"></a>

### [ ] Add Vendors who we purchase from


<a id="orgd275c87"></a>

### [ ] Allow members up to date to add orders


<a id="org75fb447"></a>

### [ ] build of a report of orders from the since the last order date split report by Vendor


<a id="orge6f15e3"></a>

## <code>[0/2]</code> Events


<a id="orgf3f561c"></a>

### [ ] Insert, Change, Delete Events


<a id="org72f8b58"></a>

### [ ] Members

